#include "Image.h"

#include <iostream>
#include <cassert>
#include  <fstream>

using namespace std;

Image::Image(){

	dimx = 0;
	dimy=0;
	tab = nullptr;
}

Image::Image(int x, int y){

	dimx =x;
	dimy=y;
	tab= new Pixel [x*y];
}

Image::~Image(){
	if (not (tab == nullptr))
        delete [] tab;
    tab = nullptr;
	dimx =0;
	dimy = 0;
}

Pixel & Image::getPix(int x, int y){
	
	return tab[y*dimx+x];
}
Pixel Image::getPix(int x, int y) const{
	return tab[y*dimx+ x];
}

void Image::setPix(int x, int y, Pixel couleur){

	//Pixel p3(10,20,45);
	getPix(x,y) = couleur;
}

void Image::dessinerRectangle(int Xmin, int Ymin, int Xmax, int Ymax, Pixel couleur){
	for(int i = Xmin; i <= Xmax; i++){
		for (int j= Ymin; j <= Ymax; j++){
			
			setPix(i,j,couleur);
		}
	}
}

void Image::afficheImage(){
	for (int i =0; i<dimx;i++){
		for (int j = 0; j < dimy; j++){
			tab[j*dimx + i].affiche();
			cout << "-";			
		}
		cout<<endl;
	}
}
		
void Image::effacer(Pixel couleur){

	dessinerRectangle(0, 0, dimx-1, dimy-1, couleur);
}

void Image::testRegression(){
	Pixel p1;
	Pixel p2(10,20,30);
	Pixel p3(1,1,1);
	Pixel p4(7,7,7);
	Image i(5,5);
	assert(p2.r == 10);
    assert(p2.g == 20);
	assert(p2.b == 30);

	assert(p3.r == 1);
    assert(p3.g == 1);
	assert(p3.b == 1);

	assert(i.dimx == 5);
    assert(i.dimy == 5);
	
	cout<<(int)(i.getPix(1,1).r)<<endl;
	i.setPix(1,1,p2);
	cout<<(int)(i.getPix(1,1).r)<<endl;

	i.afficheImage();
	i.dessinerRectangle(1,1,3,3,p3);
	cout<<"\n afficher image avec rectamgle dessine" << endl;
	i.afficheImage();
	cout<<"\n effacer image"<<endl;
	i.effacer(p4);
	i.afficheImage();
}

void Image::sauver(const string &filename) const
{
    ofstream fichier(filename.c_str());
    assert(fichier.is_open());
    fichier << "P3" << endl;
    fichier << dimx << " " << dimy << endl;
    fichier << "255" << endl;
    for ( int y = 0; y < dimy; y++)
        for ( int x = 0; x < dimx; x++)
        {
            Pixel pix = getPix(x, y);
            fichier << +pix.r << " " << +pix.g << " " << +pix.b << " ";
        }
    cout << "Sauvegarde de l'image " << filename << " ... OK\n";
    fichier.close();
}

void Image::ouvrir(const string &filename)
{
    ifstream fichier(filename.c_str());
    assert(fichier.is_open());
    char r, g, b;
    string mot;
    dimx = dimy = 0;
    fichier >> mot >> dimx >> dimy >> mot;
    assert(dimx > 0 && dimy > 0);
    if (tab != nullptr)
        delete[] tab;
    tab = new Pixel[dimx * dimy];
    for (unsigned int y = 0; y < dimy; y++)
        for (unsigned int x = 0; x < dimx; x++)
        {
            fichier >> r >> b >> g;
            getPix(x, y).r = r;
            getPix(x, y).g = g;
            getPix(x, y).b = b;
        }
    fichier.close();
    cout << "Lecture de l'image " << filename << " ... OK\n";
}

void Image::afficherConsole()const
{
    cout << dimx << " " << dimy << endl;
    for (unsigned int y = 0; y < dimy; y++)
    {
        for (unsigned int x = 0; x < dimx; x++)
        {
            Pixel pix = getPix(x, y);
            cout << +pix.r << " " << +pix.g << " " << +pix.b << " ";
        }
        cout << endl;
    }
}


