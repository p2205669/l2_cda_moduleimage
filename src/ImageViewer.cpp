#include "ImageViewer.h"

#include <iostream>
#include <cassert>
#include  <fstream>
#include <filesystem>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>


using namespace std;

ImageViewer::ImageViewer() {

    surface = nullptr;
    texture = nullptr;

    cout<<"SDL: init"<<endl;
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        cout << "Erreur lors de l'initialisation de la SDL : " << SDL_GetError() << endl;
        SDL_Quit();
        exit(EXIT_FAILURE);
    }
    cout<<"SDL_ttf: init"<<endl;
    if (TTF_Init() != 0) {
        cout << "Erreur lors de l'initialisation de la SDL_ttf : " << TTF_GetError() << endl;
        SDL_Quit();
        exit(EXIT_FAILURE);
    }
    int imgFlags = IMG_INIT_PNG | IMG_INIT_JPG;
    cout<<"SDL_image: init"<<endl;
    if(!(IMG_Init(imgFlags) && imgFlags)) {
        cout << "Erreur lors de l'initialisation de la SDL_image : " << IMG_GetError() << endl;
        SDL_Quit();
        exit(EXIT_FAILURE);
    }
	int windimx = 200;
    int windimy = 200;

    window = SDL_CreateWindow("ModuleImage", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, windimx, windimy, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
    if (window == NULL) {
        cout << "Erreur lors de la creation de la fenetre : " << SDL_GetError() << endl;
        SDL_Quit();
        exit(EXIT_FAILURE);
    }
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

}

ImageViewer::~ImageViewer() {
    TTF_Quit();
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_FreeSurface(surface);
    SDL_DestroyTexture(texture);
    surface = nullptr;
    texture = nullptr;
    SDL_Quit();
}


void ImageViewer::loadFromFile (const char* filename) {
    surface = IMG_Load(filename);
    if (surface == nullptr) {
        string nfn = string(filename);
        int n=0;
        while( (surface == nullptr) && (n<3))
        {
            nfn = string("../") + nfn;
            surface = IMG_Load(nfn.c_str());
        }
    }
    if (surface == nullptr) {
        filesystem::path currentPath = filesystem::current_path();
        cout << "Répertoire courant : " << currentPath << endl;
        cout<<"Error: cannot load "<< filename <<endl;
        exit(1);
    }

    SDL_Surface * surfaceCorrectPixelFormat = SDL_ConvertSurfaceFormat(surface,SDL_PIXELFORMAT_ARGB8888,0);
    SDL_FreeSurface(surface);
    surface = surfaceCorrectPixelFormat;

    texture = SDL_CreateTextureFromSurface(renderer,surfaceCorrectPixelFormat);
    if (texture == nullptr) {
        cout << "Error: problem to create the texture of "<< filename<< endl;
        exit(1);
    }
}

void ImageViewer::imAff(int x, int y, int w, int h)
{
    SDL_SetRenderDrawColor(renderer, 200, 200, 200, 200);
    SDL_RenderClear(renderer);

	int ok;
    SDL_Rect r;
    r.x = x;
    r.y = y;
    r.w = (w<0)?surface->w:w;
    r.h = (h<0)?surface->h:h;
    ok = SDL_UpdateTexture(texture, nullptr, surface->pixels, surface->pitch);
    assert(ok == 0);
    ok = SDL_RenderCopy(renderer, texture, nullptr, &r);
    assert(ok == 0);

}


void ImageViewer::afficher(const Image &im){

    im.sauver("./data/image3.ppm");
    loadFromFile("./data/image3.ppm");
    SDL_Event events;
	bool quit = false;

	// tant que ce n'est pas la fin ...
	while (!quit) {

		// tant qu'il y a des evenements à traiter (cette boucle n'est pas bloquante)
		while (SDL_PollEvent(&events)) {
			if (events.type == SDL_QUIT) quit = true;           // Si l'utilisateur a clique sur la croix de fermeture
			else if (events.type == SDL_KEYDOWN) 
            { 
				switch (events.key.keysym.scancode) 
                {
                    case SDL_SCANCODE_ESCAPE:
                    case SDL_SCANCODE_Q:
                        quit = true;
                        break;
                    default: break;
				}
			}
		}

        SDL_RenderPresent(renderer);
        imAff(0, 0, 200, 200);
	}


}