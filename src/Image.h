#ifndef IMAGE_H
#define _IMAGE_H
#include "Pixel.h"



/**
 * @class Image
 * 
 * @brief Une image est un tableau à 2 dimensions de largeur dimx et de hauteur dimy, dont les éléments sont des
pixels.
*/
class Image{

	private:
		Pixel * tab;
		int dimx, dimy;
	public:
		Image();
		Image(int x, int y);
		~Image();	
		//static testRegression();	
		//getPix();

		 /**
         * @brief Renvoie un lien sur le Pixel original de coordonnées (x,y).
         * 
         * @param x Abscisse du Pixel.
         * @param y Ordonnée du Pixel.
        */
		Pixel & getPix(int x, int y);
		/**
         * @brief Renvoie un lien sur une copie du Pixel de coordonnées (x,y).
         * 
         * @param x Abscisse du Pixel.
         * @param y Ordonnée du Pixel.
        */
		Pixel getPix(int x, int y)const;
		/**
         * @brief Change le Pixel des coordonnées x et y par le paramètre couleur.
         * 
         * @param x Abscisse du Pixel.
         * @param y Ordonnée du Pixel.
		 * @param couleur Couleur du nouveau Pixel.
        */
		void setPix(int x, int y, Pixel couleur);
		/**
         * @brief Dessine un rectangle dans l'image aux coordonnées indiquées.
         * 
         * @param Xmin Abscisse min du rectangle.
         * @param Ymin Ordonnée min du rectangle.
		 * @param Xmax Abscisse max du rectangle.
         * @param Ymax Ordonnée max du rectangle.
		 * @param couleur Couleur du Pixel qui rempli le rectangle déssiné.
        */
		void dessinerRectangle(int Xmin, int Ymin, int Xmax, int Ymax, Pixel couleur);
		/**
         * @brief Affiche une image sur le terminal.
        */
		void afficheImage();

		/**
         * @brief Efface l'image en la remplissant de la couleur en paramètre 
         * 
         * @param couleur couleur qui va remplir l'image a effacer.
        */
		void effacer(Pixel couleur);
		/**
         * @brief Effectue une série de tests vérifiant que toutes les fonctions fonctionnent et font bien ce qu’elles sont censées faire, ainsi que les données membres de l'objet sont conformes
         * 
         * 
        */
		static void testRegression();

		  /**
         * @brief Charge une Image depuis un fichier de nom filename.
         * 
         * @param filename Nom du fichier d'où est chargée l'Image.
        */
		void ouvrir(const string &filename);
		 /**
         * @brief Sauvegarde l'Image dans un fichier de nom filemane.
         * 
         * @param filename Nom du fichier où est sauvée l'Image.
        */
		void sauver(const string &filename) const;
		/**
         * @brief Affiche l'Image dans la console.
        */
		void afficherConsole()const;

		
};



#endif
