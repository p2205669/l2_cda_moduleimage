#ifndef PIXEL_H
#define _PIXEL_H

#include <iostream>
using namespace std;

/**
 * @struct Pixel
 * 
 * @brief Un Pixel est représenté par ses composantes r,g,b (red, green, blue).
*/

struct Pixel {

	unsigned char r,g,b;
	 /**
     * @brief Constructeur par défaut de Pixel.
    */
	Pixel();
	/**
		 * @brief Constructeur par initialisation de Pixel.
		 * 
		 * @param a1 Composante rouge du Pixel.
		 * @param a2 Composante verte du Pixel.
		 * @param a3 Composante bleue du Pixel
		*/
	Pixel(unsigned char a1, unsigned char a2, unsigned char a3 );

	void affiche();
};


#endif
