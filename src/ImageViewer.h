#ifndef IMAGE_VIEWER_H
#define _IMAGE_VIEWER_H

#include "Image.h"

#include <SDL2/SDL.h>



/**
 * @class ImageViewer
 * 
 * @brief La classe ImageViewer utilise SDL2 afin de pouvoir visualiser des Images.
*/

class ImageViewer { 

    private:
        SDL_Window * window ;
        SDL_Renderer * renderer ;
        SDL_Surface* surface;
        SDL_Texture* texture;

    public:
        /**
         * @brief Constructeur de ImageViewer. Initialise la SDL.
        */
        ImageViewer();

        /**
         * @brief Destructeur de ImageViewer. Termine la SDL.
        */
        ~ImageViewer();


        /**
         * @brief Charge l'Image depuis un fichier.
         * 
         * @param filename Nom du fichier où est stockée l'Image.
        */
        void loadFromFile (const char* filename);

        /**
         * @brief Affiche l'image chargée dans la fenêtre.
         * 
         * @param x Abscisse de l'Image dans la fenêtre.
         * @param y Ordonnée de l'Image dans la fenêtre.
         * @param w Largeur de l'Image dans la fenêtre.
         * @param h Hauteur de l'Image dans la fenêtre.
        */
        void imAff(int x, int y, int w, int h);

         /**
         * @brief Boucle d'affichage de l'Image dans une fenêtre qu'on peut quitter en appuyant sur "Q".
         * 
         * @param im de type Image qui sera affichée dans la fenêtre.
        */
        void afficher(const Image &im);

};

#endif