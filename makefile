all : bin/exemple bin/test bin/affichage

bin/exemple:  obj/Pixel.o obj/Image.o obj/mainExemple.o
	g++ -ggdb -o bin/exemple obj/mainExemple.o obj/Pixel.o obj/Image.o

bin/test: obj/mainTest.o obj/Pixel.o obj/Image.o 
	g++ -ggdb -o bin/test obj/mainTest.o obj/Pixel.o obj/Image.o

bin/affichage: obj/affichage.o obj/Pixel.o obj/Image.o obj/ImageViewer.o
	g++ -Wall obj/affichage.o obj/Pixel.o obj/Image.o obj/ImageViewer.o -lSDL2 -lSDL2_ttf -lSDL2_image -o bin/affichage

obj/affichage.o: src/mainAffichage.cpp src/Pixel.h src/Image.h src/ImageViewer.h
	g++ -Wall -c src/mainAffichage.cpp -o obj/affichage.o

obj/ImageViewer.o: src/ImageViewer.cpp
	g++ -ggdb -c src/ImageViewer.cpp -o obj/ImageViewer.o

obj/mainTest.o : src/mainTest.cpp
	g++ -ggdb -c src/mainTest.cpp -o obj/mainTest.o

obj/Pixel.o : src/Pixel.cpp
	g++ -ggdb -c src/Pixel.cpp -o obj/Pixel.o

obj/Image.o : src/Image.cpp
	g++ -ggdb -c src/Image.cpp -o obj/Image.o

obj/mainExemple.o : src/mainExemple.cpp
	g++ -ggdb -c src/mainExemple.cpp -o obj/mainExemple.o

doc: doc/doxyfile
	doxygen doc/doxyfile

clean:
	rm obj/*.o

